from typing import Annotated, TypeVar

from src.core.bases.db_manager.dao import BaseDAO

Dao = TypeVar("Dao", bound=BaseDAO)


class HelperForTests:
    """
    An helper is needed in order to optimize the work of tests, taking up less code space

    @creator: list[int] - Create N obj from taking daos and check created data with check schemas
    @deleter: None - simple deleter obj`s
    """

    async def creator(
            self,
            daos: Dao,
            dataset_schema,
            n_creates: Annotated[int, "How many users create"],
            check_schemas
    ) -> list[int]:

        id_to_delete: list[int] = []

        for _ in range(n_creates):
            row_owner = await daos.create(dataset_schema)
            assert isinstance(row_owner, check_schemas)
            id_to_delete.append(row_owner.id)
        return id_to_delete

    async def deleter(
            self,
            id_to_delete: list[int],
            daos: Dao
    ) -> None:
        for obj_id in id_to_delete:
            result = await daos.delete_obj_by_id(obj_id=obj_id)
            assert result
