import pytest


@pytest.mark.asyncio
async def test_example():
    target: int = 2
    response: int = 1 + 1
    assert target == response
