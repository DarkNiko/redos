import pytest

from src.apps.utils.requester import (AgifyRequestService, AioHTTPSession,
                                      GenderizeRequestService,
                                      NationRequestService)
from src.utils.logger_manageer import logger


@pytest.mark.asyncio
async def test_request_service():
    """---TESTING REQUESTER SERVICE"""

    request_param: dict = {"name": "John"}

    """ Create a get requests """

    age: int = await AgifyRequestService(AioHTTPSession).get_age(**request_param)
    gender: str = await GenderizeRequestService(AioHTTPSession).get_gender(**request_param)
    nation: str = await NationRequestService(AioHTTPSession).get_nation(**request_param)

    logger.info(f"REQUESTS DATA: {age} | {gender} | {nation}")

    assert isinstance(age, int)
    assert isinstance(gender, str)
    assert isinstance(nation, str)
