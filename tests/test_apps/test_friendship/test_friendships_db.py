import json

import pytest
from asyncpg import Record

from src.apps.friendship.managers import FriendShipDAO
from src.apps.friendship.schemas import (CreateFriendsSchema,
                                         FriendShipResponseSchema,
                                         ShortUserSchema, UpdateFriendsSchema)
from src.apps.user.managers import UserDAO
from src.apps.user.schemas import UserCreateSchema, UserResponseSchema
from src.db.session import async_session_asyncpg
from tests.datasets import DefaultUserDataset
from tests.test_apps.helper.helper import HelperForTests


@pytest.mark.asyncio
async def test_create():
    """--CREATE TESTS USERS--"""

    user_owner: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)

    row_owner: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_owner)

    assert isinstance(row_owner, UserResponseSchema)

    """--CREATE FRIENDSHIP DATA--"""

    friends_ids: list[int] = await HelperForTests().creator(
        daos=UserDAO(session=async_session_asyncpg),
        n_creates=2,
        dataset_schema=user_owner,
        check_schemas=UserResponseSchema
    )

    friendship_data: CreateFriendsSchema = CreateFriendsSchema(user_id=row_owner.id, friendship=friends_ids)

    row: FriendShipResponseSchema = await FriendShipDAO(session=async_session_asyncpg).create(friendship_data)

    assert isinstance(row, FriendShipResponseSchema)
    friends_ids.append(row_owner.id)

    await HelperForTests().deleter(id_to_delete=[row.id], daos=FriendShipDAO(session=async_session_asyncpg))
    await HelperForTests().deleter(id_to_delete=friends_ids, daos=UserDAO(session=async_session_asyncpg))


@pytest.mark.asyncio
async def test_get_all():
    """ Tests on get_all obj in FriendShips table """

    """ Create user and her friends """
    user_owner: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)
    row_owner: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_owner)

    friends_ids: list[int] = await HelperForTests().creator(
        daos=UserDAO(session=async_session_asyncpg),
        n_creates=2,
        dataset_schema=user_owner,
        check_schemas=UserResponseSchema
    )

    """ Create FriendShip objects """

    friendship_data: CreateFriendsSchema = CreateFriendsSchema(user_id=row_owner.id, friendship=friends_ids)
    row: FriendShipResponseSchema = await FriendShipDAO(session=async_session_asyncpg).create(friendship_data)

    assert isinstance(row, FriendShipResponseSchema)

    """ Get ALL data in Response Schema """

    response: list[FriendShipResponseSchema] = await FriendShipDAO(
        session=async_session_asyncpg
    ).get_all(to_schema=True)

    for item in response:
        assert isinstance(item, FriendShipResponseSchema)

    friends_ids.append(row_owner.id)

    await HelperForTests().deleter(id_to_delete=[row.id], daos=FriendShipDAO(session=async_session_asyncpg))
    await HelperForTests().deleter(id_to_delete=friends_ids, daos=UserDAO(session=async_session_asyncpg))


@pytest.mark.asyncio
async def test_get_detail():
    """ Test detail response with Left Join """

    """ Create user and her friends """
    user_owner: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)
    row_owner: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_owner)

    friends_ids: list[int] = await HelperForTests().creator(
        daos=UserDAO(session=async_session_asyncpg),
        n_creates=2,
        dataset_schema=user_owner,
        check_schemas=UserResponseSchema
    )

    """ Create FriendShip objects """

    friendship_data: CreateFriendsSchema = CreateFriendsSchema(user_id=row_owner.id, friendship=friends_ids)
    row: FriendShipResponseSchema = await FriendShipDAO(session=async_session_asyncpg).create(friendship_data)

    assert isinstance(row, FriendShipResponseSchema)

    response: Record = await FriendShipDAO(
        session=async_session_asyncpg
    ).get_by_user_id_with_details(user_id=row_owner.id)

    response: dict = await FriendShipDAO.a_to_dict(obj=response)

    assert isinstance(response, dict)

    """ PREPARED DATA TO SHORT INFO """

    if response.get("user_detail", None):
        response["user_detail"] = json.loads(response.get("user_detail"))
    if response.get("friendship_detail", None):
        response["friendship_detail"] = json.loads(response.get("friendship_detail"))

    friends_detail_to_schema: list[ShortUserSchema] = []

    for obj in response.get("friendship_detail"):
        friends_detail_to_schema.append(ShortUserSchema(**obj))

    response["user_detail"] = ShortUserSchema(**response.get("user_detail"))
    response["friendship_detail"] = friends_detail_to_schema

    assert FriendShipResponseSchema(**response)

    friends_ids.append(row_owner.id)

    await HelperForTests().deleter(id_to_delete=[row.id], daos=FriendShipDAO(session=async_session_asyncpg))
    await HelperForTests().deleter(id_to_delete=friends_ids, daos=UserDAO(session=async_session_asyncpg))


@pytest.mark.asyncio
async def test_update():
    """ TEST FOR UPDATED OBJ """

    """ Create user and her friends """
    user_owner: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)
    row_owner: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_owner)

    friends_ids: list[int] = await HelperForTests().creator(
        daos=UserDAO(session=async_session_asyncpg),
        n_creates=2,
        dataset_schema=user_owner,
        check_schemas=UserResponseSchema
    )

    """ Create FriendShip objects """
    friendship_data: CreateFriendsSchema = CreateFriendsSchema(user_id=row_owner.id, friendship=friends_ids)
    row: FriendShipResponseSchema = await FriendShipDAO(session=async_session_asyncpg).create(friendship_data)

    assert isinstance(row, FriendShipResponseSchema)
    assert len(row.friendship) == len(friends_ids)

    """ Create new friends """

    friends_ids_to_update: list[int] = await HelperForTests().creator(
        daos=UserDAO(session=async_session_asyncpg),
        n_creates=4,
        dataset_schema=user_owner,
        check_schemas=UserResponseSchema
    )

    obj_upd: UpdateFriendsSchema = UpdateFriendsSchema(friendship=friends_ids_to_update)
    row_update = await FriendShipDAO(session=async_session_asyncpg).update_obj(obj_id=row.id, obj_update=obj_upd)

    assert isinstance(row_update, FriendShipResponseSchema)
    assert len(row_update.friendship) == len(friends_ids_to_update)

    friends_ids.append(row_owner.id)
    friends_ids.extend(friends_ids_to_update)
    """ DELETE OBJ IN DB """
    await HelperForTests().deleter(id_to_delete=[row.id], daos=FriendShipDAO(session=async_session_asyncpg))
    await HelperForTests().deleter(id_to_delete=friends_ids, daos=UserDAO(session=async_session_asyncpg))
