import pytest
from asyncpg import Record

from src.apps.user.managers import UserDAO
from src.apps.user.schemas import (UserCreateSchema, UserResponseSchema,
                                   UserUpdateSchema)
from src.db.session import async_session_asyncpg
from src.utils.logger_manageer import logger
from tests.datasets import DefaultUserDataset
from tests.test_apps.helper.helper import HelperForTests


@pytest.mark.asyncio
async def test_create_user():
    """ Test a creating in UserDAO """

    """Create user datasets with/without emails"""
    user_data_without_email: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_without_email)
    user_data_with_email: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)

    """Create user in db"""
    row_without_email: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_data_without_email)
    row_with_email = await UserDAO(session=async_session_asyncpg).create(user_data_with_email)

    logger.info(f"RESULTS:\n{row_with_email}\n{row_without_email}")

    """GET ROW ID (USERS) FOR DELETE"""
    ids_obj: list[int] = [row_with_email.id, row_without_email.id]

    assert isinstance(row_without_email, UserResponseSchema)
    assert isinstance(row_with_email, UserResponseSchema)

    await HelperForTests().deleter(id_to_delete=ids_obj, daos=UserDAO(session=async_session_asyncpg))


@pytest.mark.asyncio
async def test_get_by_id():
    """TEST GET BY USER ID"""

    """Create user datasets with/without emails"""
    user_data_with_email: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)
    """ Create user in DB """
    row_with_email: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_data_with_email)

    assert isinstance(row_with_email, UserResponseSchema)

    logger.info(f"OBJECT:\n{row_with_email}")
    """ Check methods get_by_id """
    obj_by_id: UserResponseSchema = await UserDAO(session=async_session_asyncpg).get_by_id(obj_id=row_with_email.id)

    assert obj_by_id == row_with_email

    result = await UserDAO(session=async_session_asyncpg).delete_obj_by_id(obj_id=row_with_email.id)

    logger.info(f"RESPONSE DELETE:\n{result}")


@pytest.mark.asyncio
async def test_get_all():
    user_owner: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)

    user_ids: list[int] = await HelperForTests().creator(
        daos=UserDAO(session=async_session_asyncpg),
        n_creates=2,
        dataset_schema=user_owner,
        check_schemas=UserResponseSchema
    )

    row_to_schema: list = await UserDAO(session=async_session_asyncpg).get_all(to_schema=True)
    row: list = await UserDAO(session=async_session_asyncpg).get_all()

    assert row_to_schema
    assert row

    await HelperForTests().deleter(id_to_delete=user_ids, daos=UserDAO(session=async_session_asyncpg))


@pytest.mark.asyncio
async def test_get_by_filter():
    """
    Test Filter fields
    This test can be customized to meet your requirements and needs
    """

    """Create user datasets with/without emails"""
    user_data_with_email: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)
    """ Create user in DB """
    row_with_email: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_data_with_email)

    assert isinstance(row_with_email, UserResponseSchema)

    filter_field: dict = {"last_name": user_data_with_email.last_name, "gender": user_data_with_email.gender}

    obj: list[Record] = await UserDAO(
        session=async_session_asyncpg
    ).get_by_filter(("id", "last_name"), filter_field)
    assert obj

    result = await UserDAO(session=async_session_asyncpg).delete_obj_by_id(obj_id=row_with_email.id)
    assert result


@pytest.mark.asyncio
async def test_update():
    """ TEST TO UPDATE """
    user_data_with_email: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)

    row_with_email: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_data_with_email)

    assert row_with_email

    logger.info(f"OBJECT:\n{row_with_email}")

    obj: UserResponseSchema = await UserDAO(session=async_session_asyncpg).get_by_id(obj_id=row_with_email.id)

    obj_to_upd: UserUpdateSchema = UserUpdateSchema(**obj.model_dump())

    obj_to_upd.email = ["bebebe@bebe.com"]
    obj_to_upd.gender = "female"
    obj_to_upd.rewrite_emails = True

    obj_response: UserResponseSchema = await UserDAO(session=async_session_asyncpg).update_obj(
        obj_id=obj.id,
        obj_update=obj_to_upd
    )
    logger.info(f"UPDATED RESPONSE:\n{obj_response}")
    assert obj_response

    result = await UserDAO(session=async_session_asyncpg).delete_obj_by_id(obj_id=row_with_email.id)

    logger.info(f"DELETE RESPONSE:\n{result}")


@pytest.mark.asyncio
async def test_delete():

    user_data_with_email: UserCreateSchema = UserCreateSchema(**DefaultUserDataset.user_with_email)
    row_with_email: UserResponseSchema = await UserDAO(session=async_session_asyncpg).create(user_data_with_email)

    assert row_with_email

    logger.info(f"OBJECT:\n{row_with_email}")

    result = await UserDAO(session=async_session_asyncpg).delete_obj_by_id(obj_id=row_with_email.id)

    logger.info(f"OBJECT:\n{result}")
