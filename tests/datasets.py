
class DefaultUserDataset:

    user_without_email: dict = {
        'username': "user_one",
        'first_name': 'John',
        'second_name': 'Doe',
        'last_name': 'Smith',
        'age': 30,
        'nationality': 'American',
        'gender': 'male'
    }

    user_with_email: dict = {
        'username': "user_two",
        'email': ["tony@soprano.mafia", "gabagul@hehe.he"],
        'first_name': 'John',
        'second_name': 'Doe',
        'last_name': 'Smith',
        'age': 33,
        'nationality': 'American',
        'gender': 'male'
    }
