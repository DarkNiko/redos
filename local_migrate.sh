clear
echo "------------------------------START MIGRATION------------------------------"

export $(cat .env | xargs)

current_dir=$(pwd)

find "$current_dir/src/migrations/versions/"

files=$(ls -tr src/migrations/versions/)

while IFS= read -r file; do
  file_path="$current_dir/src/migrations/versions/$file"
  PGPASSWORD=$POSTGRES_PASSWORD psql -h "$POSTGRES_HOST" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f "$file_path"
done <<< "$files"

echo "------------------------------MIGRATION COMPLETED------------------------------"
