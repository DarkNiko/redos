clear
echo "------------------------------START INIT DATABASE------------------------------"

export $(cat .env | xargs)

PGPASSWORD=$POSTGRES_PASSWORD psql -h "$POSTGRES_HOST" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f src/migrations/init.sql
echo "------------------------------SUCCESS------------------------------"
