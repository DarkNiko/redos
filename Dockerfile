FROM python:3.11-alpine

EXPOSE 8001

RUN apk --update add curl

RUN mkdir app

WORKDIR /app

RUN python -m pip install --upgrade pip

COPY /requirements.txt .

RUN apk add --no-cache postgresql-client

RUN python -m pip install -r requirements.txt

COPY . .

RUN cd /app

RUN chmod +x src/migrations/init.sql
RUN chmod +x src/migrations/versions/
RUN chmod +x init_db.sh
RUN chmod +x migrate.sh

RUN cd ..

CMD [ "sh", "-c", "gunicorn main:app --workers 1 --timeout-keep-alive 300 --host 0.0.0.0 --port 8001" ]
