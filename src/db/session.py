
import asyncpg

from src.config import settings


class PGConnectionManager:
    def __init__(self, connection_string: str):
        self.connection_string = connection_string
        self.client = None

    async def __aenter__(self):
        self.client = await asyncpg.create_pool(
            dsn=self.connection_string,
            min_size=1,
            max_size=20,
            command_timeout=60,
        )
        return self.client

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if self.client:
            await self.client.close()


async_session_asyncpg: PGConnectionManager = PGConnectionManager(connection_string=settings.db_url)

