
from asyncpg import (CheckViolationError, ForeignKeyViolationError, Record,
                     UniqueViolationError)
from fastapi import HTTPException, status

from src.db.session import PGConnectionManager
from src.utils.logger_manageer import logger


class BaseDBCommands:  # like ORM ^_^
    """
    @__init__: _connection_pool (PGConnectionManager): Manages the pool of connections to the database.
    __check_connection(): Checks for an active database connection.

    @fetch_data(query: str, *args) -> list: Executes an SQL query and returns a list of records.

    @fetch_row(query: str, *args) -> Record: Executes an SQL query and returns the first row of the result.

    @fetch_val(query: str, *args): Executes an SQL query and returns a single value from the first row of the result.

    @execute(query: str, *args): Executes an SQL query without returning results.

    @execute_many(query: str, seq: list): Executes an SQL query for multiple sets of parameters.

    @transaction(): Returns a context manager for managing transactions.
    """

    def __init__(self, session: PGConnectionManager):
        self._connection_pool: PGConnectionManager = session

    async def __check_connection(self):
        if not self._connection_pool:
            logger.exception('Database exception: Connection error')
            raise HTTPException(status_code=status.HTTP_204_NO_CONTENT, detail="Somthing wrong")
        return self._connection_pool

    async def fetch_data(self, query: str, *args) -> list:
        async with self._connection_pool as con:
            try:
                result = await con.fetch(query, *args)
                return result
            except (ForeignKeyViolationError,
                    CheckViolationError,
                    UniqueViolationError) as e:
                logger.exception(e)
            except Exception as e:
                logger.exception(e)

    async def fetch_row(
            self,
            query: str,
            *args
    ) -> Record:
        await self.__check_connection()
        async with self._connection_pool as con:
            try:
                result = await con.fetchrow(query, *args)
                return result
            except (ForeignKeyViolationError,
                    CheckViolationError,
                    UniqueViolationError) as e:
                logger.exception(f'Database exception: {str(e)}')
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Users not found")

    async def fetch_val(self, query: str, *args):
        await self.__check_connection()
        async with self._connection_pool as con:
            try:
                result = await con.fetchval(query, *args)
                return result
            except (ForeignKeyViolationError,
                    CheckViolationError,
                    UniqueViolationError) as e:
                logger.exception(f'Database exception: {str(e)}')
            except Exception as e:
                logger.exception(e)

    async def execute(self, query: str, *args):
        await self.__check_connection()
        async with self._connection_pool as con:
            try:
                result = await con.execute(query, *args)
                return result
            except (ForeignKeyViolationError,
                    CheckViolationError,
                    UniqueViolationError) as e:
                logger.exception(f'Database exception: {str(e)}')
            except Exception as e:
                logger.exception(e)

    async def execute_many(self, query: str, seq: list):
        await self.__check_connection()
        async with self._connection_pool as con:
            try:
                result = await con.executemany(query, seq)
                return result
            except (ForeignKeyViolationError,
                    CheckViolationError,
                    UniqueViolationError) as e:
                logger.exception(f'Database exception: {str(e)}')
            except Exception as e:
                logger.exception(e)

    async def transaction(self):
        await self.__check_connection()
        async with self._connection_pool as con:
            try:
                return con.transaction
            except (ForeignKeyViolationError,
                    CheckViolationError,
                    UniqueViolationError) as e:
                logger.exception(e)
            except Exception as e:
                logger.exception(e)
