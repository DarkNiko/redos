from fastapi import HTTPException, status


class UserAlreadyExists(HTTPException):
    def __init__(self):
        detail = "User already exists"
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)


class UserNotExists(HTTPException):
    def __init__(self):
        detail = "User not found"
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=detail)
