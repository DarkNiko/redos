from src.apps.user.schemas import (UserCreateHTTPSchema, UserCreateSchema,
                                   UserResponseSchema, UserUpdateSchema)
from src.apps.utils.requester import (AgifyRequestService, AioHTTPSession,
                                      GenderizeRequestService,
                                      NationRequestService)
from src.utils.logger_manageer import logger


class UserService:

    @classmethod
    async def updater(
            cls,
            current_data: UserResponseSchema,
            updated_data: UserUpdateSchema
    ) -> UserUpdateSchema:
        if updated_data.rewrite_emails:
            return updated_data
        else:
            if updated_data.email and current_data.email:
                updated_data.email.extend(current_data.email)
            return updated_data

    @classmethod
    async def creator(cls, user_data: UserCreateHTTPSchema) -> UserCreateSchema:
        upd_user_data: dict = user_data.model_dump()
        request_param: dict = {"name": user_data.first_name}
        upd_user_data["age"] = await AgifyRequestService(AioHTTPSession).get_age(**request_param)
        upd_user_data["gender"] = await GenderizeRequestService(AioHTTPSession).get_gender(**request_param)
        upd_user_data["nationality"] = await NationRequestService(AioHTTPSession).get_nation(**request_param)
        logger.info(f"USERDATA {upd_user_data}")
        return UserCreateSchema(**upd_user_data)
