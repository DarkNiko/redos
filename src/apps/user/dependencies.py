from src.apps.user.exceptions import UserNotExists
from src.apps.user.managers import UserDAO
from src.apps.user.schemas import UserResponseSchema
from src.db.session import async_session_asyncpg


async def get_current_user(
        user_id: int
) -> UserResponseSchema | None:
    current_user: UserResponseSchema = await UserDAO(session=async_session_asyncpg).get_by_id(obj_id=user_id)
    if current_user:
        return current_user
    raise UserNotExists()
