from asyncpg import Record

from src.apps.user.schemas import UserCreateSchema, UserUpdateSchema, UserResponseSchema
from src.core.bases.db_manager.dao import BaseDAO
from src.core.bases.db_manager.utils.exceptions import ObjectNotFound


class UserDAO(BaseDAO[UserCreateSchema, UserUpdateSchema]):
    model_name: str = "users"
    response_schema = UserResponseSchema

    async def get_by_last_name(self, last_name: str) -> list[UserResponseSchema]:
        to_schemas: list[UserResponseSchema] = []
        response: list = await self.get_by_filter(fields=("*",), filter_field={"last_name": last_name})
        if response:
            for obj in response:
                to_schemas.append(UserResponseSchema(**obj))
            return to_schemas
        raise ObjectNotFound(model=self.model_name)
