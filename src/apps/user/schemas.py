from typing import Optional

from pydantic import BaseModel, Field
from pydantic.v1 import validator

from src.apps.user.enums import GenderEnum

"""
(
    id               SERIAL PRIMARY KEY,
    first_name       VARCHAR           DEFAULT NULL,
    second_name      VARCHAR           DEFAULT NULL,
    last_name        VARCHAR           DEFAULT NULL,
    email            JSONB             DEFAULT NULL,
    phone            VARCHAR           DEFAULT NULL,
    country          VARCHAR           DEFAULT NULL,
    username         VARCHAR           DEFAULT NULL,
    nationality      VARCHAR           DEFAULT NULL,
    created_at       TIMESTAMPTZ       DEFAULT now(),
    updated_at       TIMESTAMPTZ       DEFAULT now()
);
"""


class BaseUserScheme(BaseModel):
    first_name: str
    second_name: str
    last_name: str
    email: Optional[list] = None
    username: Optional[str] = None
    nationality: str
    age: int
    gender: str

    @validator('gender')
    def validate_gender(cls, gender):
        if gender not in [e.value for e in GenderEnum]:
            raise ValueError(f"Invalid value for gender: {gender}")
        return gender


class UserCreateSchema(BaseUserScheme):
    ...


class UserCreateHTTPSchema(BaseModel):
    email: Optional[list[str]] = Field(None)
    username: Optional[str] = Field(None)
    first_name: str
    second_name: str
    last_name: str


class UserUpdateSchema(BaseUserScheme):
    first_name: Optional[str] = None
    second_name: Optional[str] = None
    last_name: Optional[str] = None
    nationality: Optional[str] = None
    age: Optional[int] = None
    gender: Optional[str] = None

    rewrite_emails: Optional[bool] = Field(
        False,
        description="this check box is for adding to current emails or rewriting"
    )


class UserUpdateDB(BaseUserScheme):
    ...


class UserResponseSchema(BaseUserScheme):
    id: int

