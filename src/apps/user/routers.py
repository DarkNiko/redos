from fastapi import status
from fastapi.routing import APIRoute, APIRouter

from src.apps.user import handlers
from src.apps.user.schemas import UserResponseSchema

user_routes = [
    APIRoute(
        path="/all",
        endpoint=handlers.get_all,
        methods=["GET"],
        response_model=list[UserResponseSchema],
        status_code=status.HTTP_200_OK,
        summary="Get my user data",
        description="Return current user data.(Authorized)",
    ),
    APIRoute(
        path="/{user_id}",
        endpoint=handlers.get_by_id,
        methods=["GET"],
        response_model=UserResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
    APIRoute(
        path="/get_by_last_name/",
        endpoint=handlers.get_by_last_name,
        methods=["GET"],
        response_model=list[UserResponseSchema],
        status_code=status.HTTP_200_OK,
    ),
    APIRoute(
        path="/{user_id}",
        endpoint=handlers.update_user,
        methods=["PATCH"],
        response_model=UserResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
    APIRoute(
        path="/create",
        endpoint=handlers.create_user,
        methods=["POST"],
        response_model=UserResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
]
user_router = APIRouter(prefix="/users", tags=["User"])
user_router.include_router(APIRouter(routes=user_routes))
