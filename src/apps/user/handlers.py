from fastapi import Depends
from starlette.responses import JSONResponse

from src.apps.user import exceptions

from src.apps.user.dependencies import get_current_user
from src.apps.user.managers import UserDAO
from src.apps.user.schemas import (
    UserResponseSchema,
    UserUpdateSchema,
    UserCreateSchema,
    UserCreateHTTPSchema
)
from src.apps.user.service import UserService
from src.db.session import async_session_asyncpg


async def update_user(
    user_update: UserUpdateSchema,
    current_user: UserResponseSchema = Depends(get_current_user),
) -> UserResponseSchema:
    if not current_user:
        raise exceptions.UserNotExists
    user_update: UserUpdateSchema = await UserService.updater(current_data=current_user, updated_data=user_update)
    return await UserDAO(session=async_session_asyncpg).update_obj(obj_id=current_user.id, obj_update=user_update)


async def create_user(
    user_data: UserCreateHTTPSchema
) -> UserResponseSchema:
    user_prepared_data: UserCreateSchema = await UserService.creator(user_data=user_data)
    return await UserDAO(session=async_session_asyncpg).create(data=user_prepared_data)


async def delete_user(
        user_id: int
):
    await UserDAO(session=async_session_asyncpg).delete_obj_by_id(obj_id=user_id)
    return JSONResponse(status_code=200, content={"detail": f"{UserDAO.model_name} obj deleted successfully"})


async def get_by_id(user_id: int) -> UserResponseSchema:
    return await UserDAO(session=async_session_asyncpg).get_by_id(obj_id=user_id)


async def get_all() -> list[UserResponseSchema]:
    return await UserDAO(session=async_session_asyncpg).get_all(to_schema=True)


async def get_by_last_name(user_last_name: str) -> list[UserResponseSchema]:
    return await UserDAO(session=async_session_asyncpg).get_by_last_name(last_name=user_last_name)

