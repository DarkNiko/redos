from typing import Optional

from pydantic import BaseModel

"""
(
    id               SERIAL PRIMARY KEY,
    user_id          INT REFERENCES users.id UNIQUE,
    friendship       INT[]             DEFAULT NULL,
    created_at       TIMESTAMPTZ       DEFAULT now(),
    updated_at       TIMESTAMPTZ       DEFAULT now()
);
"""


class ShortUserSchema(BaseModel):
    id: int
    first_name: str
    last_name: str


class BaseFriendShipsSchema(BaseModel):
    user_id: int
    friendship: list[int]


class FriendShipResponseSchema(BaseFriendShipsSchema):
    id: int
    user_detail: Optional[ShortUserSchema] = None
    friendship_detail: Optional[list[ShortUserSchema]] = None


class CreateFriendsSchema(BaseFriendShipsSchema):
    ...


class UpdateFriendsSchema(BaseModel):
    friendship: list[int]


class DeleteFriendSchema(BaseModel):
    user_id: int
    friends_delete_id: int
