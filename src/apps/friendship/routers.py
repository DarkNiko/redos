from fastapi import status
from fastapi.routing import APIRoute, APIRouter

from src.apps.friendship import handlers
from src.apps.friendship.schemas import FriendShipResponseSchema

friendship_routes = [
    APIRoute(
        path="/all",
        endpoint=handlers.get_all,
        methods=["GET"],
        response_model=list[FriendShipResponseSchema],
        status_code=status.HTTP_200_OK,
        summary="Get my user data",
        description="Return current user data.(Authorized)",
    ),
    APIRoute(
        path="/{obj_id}",
        endpoint=handlers.get_by_id,
        methods=["GET"],
        response_model=FriendShipResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
    APIRoute(
        path="/by_user/{user_id}",
        endpoint=handlers.get_by_user_id,
        methods=["GET"],
        response_model=FriendShipResponseSchema,
        status_code=status.HTTP_200_OK,
    ),
    APIRoute(
        path="/by_user/detail/{user_id}",
        endpoint=handlers.get_by_user_id_detail,
        methods=["GET"],
        response_model=FriendShipResponseSchema,
        status_code=status.HTTP_200_OK,
    ),
    APIRoute(
        path="/update/{user_id}",
        endpoint=handlers.update_friendship,
        methods=["PATCH"],
        response_model=FriendShipResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
    APIRoute(
        path="/delete_friends/{user_id}",
        endpoint=handlers.delete_friends,
        methods=["PATCH"],
        response_model=FriendShipResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
    APIRoute(
        path="/create",
        endpoint=handlers.create_friendship,
        methods=["POST"],
        response_model=FriendShipResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
]
friendship_router = APIRouter(prefix="/friendship", tags=["Friendship Relation"])
friendship_router.include_router(APIRouter(routes=friendship_routes))
