from fastapi import Depends
from starlette.responses import JSONResponse

from src.apps.friendship.dependencies import get_current_friendship_obj
from src.apps.friendship.exceptions import AlreadyExists, EmptyDataError
from src.apps.friendship.managers import FriendShipDAO
from src.apps.friendship.schemas import (CreateFriendsSchema,
                                         FriendShipResponseSchema,
                                         UpdateFriendsSchema)
from src.apps.friendship.service import UserFriendsService
from src.apps.user.exceptions import UserNotExists
from src.db.session import async_session_asyncpg
from src.utils.logger_manageer import logger


async def update_friendship(
    upd_friendship: UpdateFriendsSchema,
    current_friendship: FriendShipResponseSchema = Depends(get_current_friendship_obj),
) -> FriendShipResponseSchema:

    if not upd_friendship.friendship:
        raise EmptyDataError()

    prepare_data: UpdateFriendsSchema = await UserFriendsService.prepare_updated_data(
        data=upd_friendship,
        current_data=current_friendship
    )

    return await FriendShipDAO(
        session=async_session_asyncpg
    ).update_obj(obj_id=current_friendship.id, obj_update=prepare_data)


async def create_friendship(
    data: CreateFriendsSchema,
) -> FriendShipResponseSchema:
    obj_by_user: FriendShipResponseSchema = await get_current_friendship_obj(user_id=data.user_id)

    if obj_by_user:
        raise AlreadyExists()

    await UserFriendsService.check_exists(data)
    try:
        return await FriendShipDAO(session=async_session_asyncpg).create(data=data)
    except Exception as err:
        print("TUT")
        logger.exception(str(err))
        raise UserNotExists()


async def delete_friendship(
        user_id: int
):
    await FriendShipDAO(session=async_session_asyncpg).delete_obj_by_id(obj_id=user_id)
    return JSONResponse(status_code=200, content={"detail": f"{FriendShipDAO.model_name} obj deleted successfully"})


async def get_by_id(obj_id: int) -> FriendShipResponseSchema:
    return await FriendShipDAO(session=async_session_asyncpg).get_by_id(obj_id=obj_id)


async def get_by_user_id(user_id: int) -> FriendShipResponseSchema:
    response: FriendShipResponseSchema = await FriendShipDAO(
        session=async_session_asyncpg
    ).get_by_user_id(user_id=user_id)
    if response:
        return response
    raise UserNotExists()


async def get_by_user_id_detail(user_id: int) -> FriendShipResponseSchema:
    response: FriendShipResponseSchema = await UserFriendsService.prepare_detail_data(
        await FriendShipDAO(session=async_session_asyncpg).get_by_user_id_with_details(user_id=user_id)
    )
    return response


async def get_all() -> list[FriendShipResponseSchema]:
    return await FriendShipDAO(session=async_session_asyncpg).get_all(to_schema=True)


async def delete_friends(
        upd_friendship: UpdateFriendsSchema,
        current_friendship: FriendShipResponseSchema = Depends(get_current_friendship_obj),
) -> FriendShipResponseSchema:

    if not upd_friendship.friendship:
        raise EmptyDataError()

    if not current_friendship:
        raise UserNotExists()

    prepare_data: UpdateFriendsSchema = await UserFriendsService.prepare_delete_friend_data(
        data=upd_friendship,
        current_data=current_friendship
    )

    return await FriendShipDAO(
        session=async_session_asyncpg
    ).update_obj(obj_id=current_friendship.id, obj_update=prepare_data)
