from fastapi import HTTPException, status


class EmptyDataError(HTTPException):
    def __init__(self):
        detail = "It is not possible to update with empty data"
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail=detail)


class AlreadyExists(HTTPException):
    def __init__(self):
        detail = "Obj with current user-id already exists"
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail=detail)


class FriendsNotExists(HTTPException):
    def __init__(self):
        detail = "Your friends is not exists, T_T"
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=detail)


class UnknownException(HTTPException):
    def __init__(self):
        detail = "Sorry, I do not know what the problem is. Contact technical support"
        super().__init__(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=detail)
