
from src.apps.friendship.managers import FriendShipDAO
from src.apps.friendship.schemas import FriendShipResponseSchema
from src.db.session import async_session_asyncpg


async def get_current_friendship_obj(
        user_id: int
) -> FriendShipResponseSchema | None:
    current_obj: FriendShipResponseSchema = await FriendShipDAO(
        session=async_session_asyncpg
    ).get_by_user_id(user_id=user_id)
    if current_obj:
        return current_obj
    return None

# raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Object not found")
