from src.apps.friendship.schemas import FriendShipResponseSchema, CreateFriendsSchema, UpdateFriendsSchema
from src.core.bases.db_manager.dao import BaseDAO
from src.core.bases.db_manager.utils.exceptions import ObjectNotFound


class FriendShipDAO(BaseDAO[CreateFriendsSchema, UpdateFriendsSchema]):
    model_name: str = "friendships"
    response_schema = FriendShipResponseSchema

    async def get_by_user_id(self, user_id: int) -> FriendShipResponseSchema | None:
        response: list = await self.get_by_filter(fields=("*",), filter_field={"user_id": user_id})
        if response:
            for item in response:
                return FriendShipResponseSchema(**item)
        return

    async def get_by_user_id_with_details(self, user_id: int):
        sql_join_row: str = f"""
        SELECT f.id, f.user_id, f.friendship, 
        json_build_object('id', u.id, 'first_name', u.first_name, 'last_name', u.last_name) AS user_detail, 
        json_agg(json_build_object('id', u2.id, 'first_name', u2.first_name, 'last_name', u2.last_name)) AS friendship_detail
        FROM 
            public.friendships f
        LEFT JOIN 
            public.users u ON u.id = f.user_id
        LEFT JOIN 
            public.users u2 ON u2.id = ANY(f.friendship)
        WHERE 
            f.user_id = {user_id}
        GROUP BY 
            f.id, f.user_id, u.id, u.first_name, u.last_name;
        """
        result = await self.pg_client.fetch_row(sql_join_row)
        if result:
            return result
        raise ObjectNotFound(model=self.model_name)
