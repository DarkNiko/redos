import json

from asyncpg import Record
from pydantic import ValidationError

from src.apps.friendship.exceptions import FriendsNotExists, UnknownException
from src.apps.friendship.managers import FriendShipDAO
from src.apps.friendship.schemas import (FriendShipResponseSchema,
                                         ShortUserSchema, UpdateFriendsSchema)
from src.apps.user.dependencies import get_current_user
from src.utils.logger_manageer import logger


class UserFriendsService:

    @classmethod
    async def prepare_updated_data(
            cls,
            data: UpdateFriendsSchema,
            current_data: FriendShipResponseSchema
    ) -> UpdateFriendsSchema:
        await cls.check_exists(check_data=data)
        data.friendship.extend(current_data.friendship)
        return data

    @classmethod
    async def prepare_delete_friend_data(
            cls,
            data: UpdateFriendsSchema,
            current_data: FriendShipResponseSchema
    ) -> UpdateFriendsSchema:

        await cls.check_exists(check_data=data)

        for i in data.friendship:
            index_id = current_data.friendship.index(i)
            current_data.friendship.pop(index_id)

        data.friendship = current_data.friendship

        return data

    @classmethod
    async def check_exists(cls, check_data):
        try:
            for user_id in check_data.friendship:
                await get_current_user(user_id)
        except Exception as e:
            logger.exception(str(e))
            raise FriendsNotExists()

    @classmethod
    async def prepare_detail_data(cls, data: Record) -> FriendShipResponseSchema:

        data: dict = await FriendShipDAO.a_to_dict(obj=data)

        if data.get("user_detail", None):
            data["user_detail"] = json.loads(data.get("user_detail"))
        if data.get("friendship_detail", None):
            data["friendship_detail"] = json.loads(data.get("friendship_detail"))

        friends_detail_to_schema: list[ShortUserSchema] = []

        for obj in data.get("friendship_detail"):
            friends_detail_to_schema.append(ShortUserSchema(**obj))

        data["user_detail"] = ShortUserSchema(**data.get("user_detail"))
        data["friendship_detail"] = friends_detail_to_schema
        try:
            return FriendShipResponseSchema(**data)
        except ValidationError as e:
            logger.exceptio(str(e))
            raise UnknownException()
