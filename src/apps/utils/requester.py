import asyncio
import random
from typing import Any

import aiohttp


class BaseRequestService:
    api_url: str | None = None
    mock_headers: dict = {"User-Agent": "PostmanRuntime/7.29.4"}

    async def __get_request(self, session, **params):
        raise NotImplementedError

    async def __post_request(self, session):
        raise NotImplementedError


class AioHTTPSession(BaseRequestService):

    def __init__(self, api_url):
        self.api_url: str = api_url

    async def task_requester(self, **params) -> tuple[Any]:
        tasks: list = []
        async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=6, sock_read=4),
                headers=self.mock_headers
        ) as session:
            tasks.append(asyncio.create_task(self.__get_request(session, **params)))
            response = await asyncio.gather(*tasks)
        return response

    async def __get_request(self, session: aiohttp.ClientSession, **params):
        try:
            async with session.get(self.api_url, params=params) as response:
                return await response.json()
        except aiohttp.ServerTimeoutError:
            pass


class AgifyRequestService(BaseRequestService):
    api_url: str = "https://api.agify.io/"
    description: str = "Determine age"

    def __init__(self, client_handler):
        self.client_handler = client_handler(self.api_url)

    async def get_age(self, **params) -> int:
        response: list | None = await self.client_handler.task_requester(**params)

        if response:
            return response[0].get("age")
        raise Exception(f"Probably with get age from {self.api_url}")


class GenderizeRequestService(BaseRequestService):
    api_url = "https://api.genderize.io/"
    description: str = "Determine gender"

    def __init__(self, client_handler):
        self.client_handler = client_handler(self.api_url)

    async def get_gender(self, **params):
        response = await self.client_handler.task_requester(**params)

        if response:
            return response[0].get("gender")
        raise Exception(f"Probably with get age from {self.api_url}")


class NationRequestService(BaseRequestService):
    api_url = "https://api.nationalize.io/"
    description: str = "Determine nation"

    def __init__(self, client_handler):
        self.client_handler = client_handler(self.api_url)

    async def get_nation(self, **params):
        response = await self.client_handler.task_requester(**params)

        if response:
            country_code: dict = random.choice(response[0].get("country"))
            return country_code.get("country_id")
        raise Exception(f"Probably with get age from {self.api_url}")
