import os.path

from src.config import LOGS_SLICER, LOGS_DIR, FILENAME_LOGS
from src.utils.logger_manageer import logger


class LogManager:
    log_slice: int = int(LOGS_SLICER)
    path_to_file = os.path.join(LOGS_DIR, FILENAME_LOGS)

    @classmethod
    async def get_last_lines_from_log(cls) -> list[str]:
        try:
            with open(cls.path_to_file, 'r') as file:
                lines = file.readlines()
            return lines[-cls.log_slice:]
        except Exception as err:
            logger.exception(err)
