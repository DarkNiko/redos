
from src.apps.log_stream.schemas import LogsInfoResponseSchema
from src.apps.log_stream.service import LogManager


async def read_last_log_lines() -> LogsInfoResponseSchema:
    lines: list[str] = await LogManager.get_last_lines_from_log()
    return LogsInfoResponseSchema(log_lines=lines)
