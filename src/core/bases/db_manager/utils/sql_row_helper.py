
class RowHelper:
    """
    Helper to parse data and construct Query request
    """

    block_word: list = ["SELECT", "DELETE", "UPDATE"]
    methods_get: list = ["SELECT", "DELETE"]
    methods_post_put: list = ["INSERT"]

    @classmethod
    async def tuple_elements_to_string(cls, obj: tuple) -> str:
        one_element_generate_obj = ('\'{}\''.format(item) for item in obj)
        obj_to_str: str = ", ".join(one_element_generate_obj)
        return obj_to_str

    @classmethod
    async def old_prepared_data(cls, data) -> tuple[str, tuple, list]:
        array: list = []
        data: dict = data.model_dump()

        if isinstance(data.get("rewrite_emails", None), bool):
            data.pop("rewrite_emails")

        data = {key: value for key, value in data.items() if value is not None}

        fields_str = ', '.join(tuple(data.keys()))

        for val in data.values():
            if isinstance(val, list):
                array.append(val)

        values: tuple = tuple(
            value for value in data.values()
            if not isinstance(value, list) or isinstance(value, str) and value not in cls.block_word
        )

        return fields_str, values, array

    @classmethod
    async def prepared_with_condition(cls, data, methods: str) -> tuple[str, str, tuple]:
        data: dict = {key: value for key, value in data.model_dump().items() if value is not None}
        fields_str: str = ', '.join(tuple(data.keys()))
        conditions: list = []
        conditions_str: str = ""

        if isinstance(data.get("rewrite_emails", None), bool):
            data.pop("rewrite_emails")

        for i, key in enumerate(data.keys(), start=1):
            if methods in cls.methods_post_put:
                conditions.append(f"${i}")
            elif methods in cls.methods_get or methods == "UPDATE":
                conditions.append(f"{key}=${i}")

        if methods in cls.methods_get:
            conditions_str: str = " AND ".join(conditions)
        elif methods in cls.methods_post_put or methods == "UPDATE":
            conditions_str: str = ", ".join(conditions)

        values = tuple(data.values())

        return fields_str, conditions_str, values
