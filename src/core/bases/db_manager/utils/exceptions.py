from fastapi import HTTPException
from starlette import status


class DataBaseHTTPException(HTTPException):
    def __init__(self):
        detail = "The user is already exists."
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)


class DataBaseLocalException(Exception):
    def __init__(self):
        detail = "Database error"
        super().__init__(detail)


class ObjectNotFound(HTTPException):
    def __init__(self, model: str):
        detail = f"Database err model: '{model}' obj not found"
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=detail)


class ObjectCantUpdated(HTTPException):
    def __init__(self, model: str):
        detail = f"Database update error model: '{model}'"
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=detail)
