from typing import Generic, TypeVar

from asyncpg import Record
from pydantic import BaseModel, ValidationError

from src.core.bases.db_manager.utils.exceptions import (DataBaseLocalException,
                                                        ObjectCantUpdated,
                                                        ObjectNotFound)
from src.core.bases.db_manager.utils.sql_row_helper import RowHelper
from src.db.pg_manager import BaseDBCommands

CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)
ResponseSchemaType = TypeVar("ResponseSchemaType", bound=BaseModel)


class BaseDAO(Generic[CreateSchemaType, UpdateSchemaType]):
    model_name: str | None = None
    response_schema: ResponseSchemaType | None = None

    def __init__(
            self,
            session,
    ):
        self.session = session
        self.pg_client: BaseDBCommands = BaseDBCommands(session=self.session)

    async def old_create(self, data: CreateSchemaType) -> ResponseSchemaType:
        try:
            fields, values, array = await RowHelper.old_prepared_data(data=data)
            sql_query: str = f"INSERT INTO {self.model_name} ({fields}) VALUES {values} RETURNING *;"

            if array:
                values: str = await RowHelper.tuple_elements_to_string(obj=values)
                sql_query = (f"INSERT INTO {self.model_name}"
                             f" ({fields}) VALUES ({', '.join([f'ARRAY{value}' for value in array])}, {values})"
                             f" RETURNING *;")

            row: Record = await self.pg_client.fetch_row(query=sql_query)

            if row:
                return self.response_schema.model_validate({**row})
            raise DataBaseLocalException()
        except (ValidationError, Exception, DataBaseLocalException) as e:
            print(e)

    async def create(self, data: CreateSchemaType) -> ResponseSchemaType:
        fields, condition, values = await RowHelper.prepared_with_condition(data=data, methods="INSERT")
        sql_query: str = f"INSERT INTO {self.model_name} ({fields}) VALUES ({condition}) RETURNING *;"
        row: Record = await self.pg_client.fetch_row(sql_query, *values)

        if row:
            return self.response_schema.model_validate({**row})
        raise DataBaseLocalException()

    async def get_by_id(self, obj_id: int) -> dict | ResponseSchemaType:
        sql_query: str = f"SELECT * FROM {self.model_name} WHERE id = {obj_id}"
        row: Record = await self.pg_client.fetch_row(query=sql_query)

        if row:
            return self.response_schema.model_validate({**row})
        raise ObjectNotFound(model=self.model_name)

    async def get_all(
            self,
            to_schema: bool = False
    ) -> list | None:

        result_to_schemas: list = []

        sql_query: str = f"SELECT * FROM {self.model_name};"

        row: list[Record] = await self.pg_client.fetch_data(query=sql_query)
        if to_schema:
            for obj in row:
                result_to_schemas.append(self.response_schema.model_validate({**obj}))
            return result_to_schemas
        else:
            return row

    async def get_by_filter(self, fields: tuple, filter_field: dict):
        fields_str: str = ", ".join(fields)
        conditions: list = []

        for i, key in enumerate(filter_field.keys(), start=1):
            conditions.append(f"{key} = ${i}")

        conditions_str = " AND ".join(conditions)

        values = tuple(filter_field.values())
        sql_query: str = f"SELECT {fields_str} FROM {self.model_name} WHERE {conditions_str}"
        row: list[Record] = await self.pg_client.fetch_data(sql_query, *values)

        if row:
            return row
        return

    async def old_update_obj(self, obj_id: int, obj_update: UpdateSchemaType):
        try:
            fields, values, array = await RowHelper.old_prepared_data(data=obj_update)
            sql_query: str = f"UPDATE {self.model_name} SET ({fields}) = {values} WHERE id = {obj_id} RETURNING *;"

            if array:
                values: str = await RowHelper.tuple_elements_to_string(obj=values)
                sql_query = (f"UPDATE {self.model_name}"
                             f" SET ({fields}) = ({', '.join([f'ARRAY{value}' for value in array])}, {values})"
                             f" WHERE id = {obj_id} RETURNING *;")

            row: Record = await self.pg_client.fetch_row(sql_query)

            if row:
                return self.response_schema.model_validate({**row})

            raise ObjectCantUpdated(model=self.model_name)
        except Exception as err:
            print(err)

    async def update_obj(self, obj_id: int, obj_update: UpdateSchemaType):
        fields, condition, values = await RowHelper.prepared_with_condition(data=obj_update, methods="UPDATE")

        sql_query: str = f"UPDATE {self.model_name} SET {condition} WHERE id = {obj_id} RETURNING *;"
        print(sql_query)
        row: Record = await self.pg_client.fetch_row(sql_query, *values)

        if row:
            return self.response_schema.model_validate({**row})
        raise ObjectCantUpdated(model=self.model_name)

    async def delete_obj_by_id(self, obj_id: int):
        sql_query: str = f"DELETE FROM {self.model_name} WHERE id = {obj_id}"
        result = await self.pg_client.execute(sql_query)
        return result

    async def _check_exists(self, prepared_data: dict) -> bool:
        pass

    @classmethod
    async def a_to_dict(cls, obj: Record) -> dict:
        result: dict = {key: value for key, value in obj.items() if not key.startswith('_')}
        return result
