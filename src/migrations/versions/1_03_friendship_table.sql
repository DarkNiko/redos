BEGIN;

CREATE TABLE friendships
(
    id               SERIAL PRIMARY KEY,
    user_id          INT REFERENCES public.users(id) UNIQUE,
    friendship       INT[]             DEFAULT NULL,
    created_at       TIMESTAMPTZ       DEFAULT now(),
    updated_at       TIMESTAMPTZ       DEFAULT now()
);

COMMIT;