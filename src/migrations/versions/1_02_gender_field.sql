
CREATE TYPE gender_enum AS ENUM ('male', 'female');

ALTER TABLE users
ADD COLUMN gender gender_enum;

INSERT INTO migration_version (version)
VALUES ('1__02_gender_field.sql');

COMMIT;