BEGIN;

CREATE OR REPLACE FUNCTION update_column_updated()
    RETURNS TRIGGER AS
$$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TABLE users
(
    id               SERIAL PRIMARY KEY,
    first_name       VARCHAR           DEFAULT NULL,
    second_name      VARCHAR           DEFAULT NULL,
    last_name        VARCHAR           DEFAULT NULL,
    email            TEXT[]            DEFAULT NULL,
    username         VARCHAR           DEFAULT NULL,
    nationality      VARCHAR           DEFAULT NULL,
    created_at       TIMESTAMPTZ       DEFAULT now(),
    updated_at       TIMESTAMPTZ       DEFAULT now()
);

CREATE TABLE migration_version
(
    id               SERIAL PRIMARY KEY,
    version       VARCHAR           DEFAULT NULL
);

COMMIT;

