import csv
import os
import io
from io import BytesIO

from zipfile import ZipFile

from fastapi import File


class FileManager:

    @classmethod
    async def save_files(cls, file_to_save: File, order_id: str) -> str:
        pass

    @classmethod
    async def zipper_files(cls, dispute_files: list) -> BytesIO:
        in_memory = io.BytesIO()

        async with ZipFile(in_memory, "w") as zipf:
            for file_path in dispute_files:
                zipf.write("/app" + file_path, arcname=os.path.basename(file_path))

        in_memory.seek(0)

        return in_memory

    @classmethod
    async def read_zip_files(cls, zip_file: File) -> tuple[str, ZipFile] | None:
        csv_filename: str = None
        zip_archive: ZipFile = ZipFile(io.BytesIO(await zip_file.read()))

        for filename in zip_archive.namelist():
            if filename.endswith('.csv'):
                csv_filename = filename
                break

        if not csv_filename:
            return None
        return csv_filename, zip_archive

    @classmethod
    async def read_csv_from_zip(cls, zip_files: ZipFile, csv_filename: str) -> list[str]:
        try:
            with zip_files.open(csv_filename) as csvfile:
                reader = csv.reader(io.TextIOWrapper(csvfile))
                links: list[str] = [row[0] for row in reader if row]
                return list(set(links))
        except Exception as err:
            print("ERR", err)


