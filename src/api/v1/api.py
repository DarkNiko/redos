from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.apps.friendship.routers import friendship_router
from src.apps.log_stream.routers import log_router
from src.apps.user.routers import user_router
from src.config import (ALLOW_CREDENTIALS, BACKEND_CORS_ORIGINS, HEADERS,
                        METHODS, settings)
from src.version import VERSION

PROJECT_NAME = settings.PROJECT_NAME
PROJECT_DESCRIPTION = settings.PROJECT_DESCRIPTION
ENVIRONMENT = settings.ENVIRONMENT

apiv1 = FastAPI(
    title=PROJECT_NAME,
    description=PROJECT_DESCRIPTION,
    version=VERSION,
)
apiv1.add_middleware(
    CORSMiddleware,
    allow_origins=BACKEND_CORS_ORIGINS,
    allow_credentials=ALLOW_CREDENTIALS,
    allow_methods=METHODS,
    allow_headers=HEADERS,
)


apiv1.include_router(user_router)
apiv1.include_router(log_router)
apiv1.include_router(friendship_router)
