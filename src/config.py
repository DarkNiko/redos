import os
from pathlib import Path

from dotenv import load_dotenv
from pydantic_settings import BaseSettings, SettingsConfigDict

load_dotenv()
BASE_DIR: Path = Path(__file__).parent

ALLOW_CREDENTIALS = True
BACKEND_CORS_ORIGINS = ["*"]
METHODS = ["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"]
HEADERS = [
    "Content-Type",
    "Set-Cookie",
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Origin",
    "Authorization",
]
DB_NAMING_CONVENTION = {
    "all_column_names": lambda constraint, table: "_".join(
        [column.name for column in constraint.columns.values()]
    ),
    "ix": "ix__%(table_name)s__%(all_column_names)s",
    "uq": "uq__%(table_name)s__%(all_column_names)s",
    "ck": "ck__%(table_name)s__%(constraint_name)s",
    "fk": "fk__%(table_name)s__%(all_column_names)s__%(referred_table_name)s",
    "pk": "pk__%(table_name)s",
}


class AppSettings(BaseSettings):
    PROJECT_NAME: str = "Demo app"
    PROJECT_DESCRIPTION: str = "description"
    ENVIRONMENT: str = "local"
    SECRET_KEY: str = ""
    RESET_SECRET_KEY: str = ""
    OPENAPI_URL: str = ""
    BASEURL: str = ""
    BASEADMINURL: str = f"{BASEURL}/admin"

    TELEBOT_TOKEN: str | None = None
    LOG_TG_CHANNEL: str | None = None
    NOTIFY_TG_CHANNEL: str | None = None
    SEND_LOGS: bool = False
    SEND_ERRORS: bool = False
    LOG_DELAY: float = 0.7

    POSTGRES_HOST: str = "localhost"
    POSTGRES_PORT: int = 5432
    POSTGRES_USER: str = "postgres"
    POSTGRES_PASSWORD: str = "postgres"
    POSTGRES_DB: str = "DB_NAME"

    # flake8: noqa: C901
    @property
    def db_url(self):
        DATABASE_URL = f"postgresql://{self.POSTGRES_USER}:{self.POSTGRES_PASSWORD}@{self.POSTGRES_HOST}:{self.POSTGRES_PORT}/{self.POSTGRES_DB}"
        return DATABASE_URL

    DEBUG: bool = False
    JWT_ALGORITHM: str = "HS256"
    TOKEN_LIFETIME_HOURS: int = 24 * 30
    RESET_TOKEN_LIFETIME_HOURS: int = 24 * 30

    ACCESS_TOKEN_EXPIRE_MINUTES: int = 24 * 60
    REFRESH_TOKEN_EXPIRE_DAYS: int = 30
    COOKIE_NAME: str = "session_token"
    VERIFY_USER_TOKEN_AUDIENCE: str = "fastapi-users:verify"
    RESET_PASSWORD_TOKEN_AUDIENCE: str = "fastapi-users:reset"

    model_config = SettingsConfigDict(env_file=".env", extra="allow")


settings = AppSettings()


STATIC_DIR: Path = BASE_DIR / "static"
STATIC_DIR.mkdir(exist_ok=True)
MEDIA_DIR: Path = STATIC_DIR / "media"
MEDIA_DIR.mkdir(exist_ok=True)
TEMPLATES_DIR: Path = BASE_DIR / "templates"
TEMPLATES_DIR.mkdir(exist_ok=True)
LOGS_DIR: Path = BASE_DIR / "logs"
LOGS_DIR.mkdir(exist_ok=True)

FILENAME_LOGS: str = "app.log"

LOGS_SLICER: int = os.environ.get("LOG_SLICER", 20)
TG_CAPTION_LENGHT = 1024
TG_POST_LENGHT = 4096
TG_MAX_DAYS = 180
