# REDOS №1 in Russia

Тестовое задание:

Общая задача: сделать получение информации о людях по фамилии.

Необходимо:

Обязательная часть:

Создать REST API, который будет предоставлять информацию о человеке.

Внутри базы данных должна быть одна таблица — информация о человеке. Сводная информация о человеке: ФИО, пол, национальность, возраст.

Должно быть не менее 10 человек.

При создании человека внутри базы данных необходимо получать возраст, пол и национальность о человеке с помощью сторонних ресурсов: agify.io, genderize.io, nationalize.io.

Дополнительная часть:

1. Добавить в систему возможность хранить электронные почтовые ящики пользователя. Почтовых ящиков может быть несколько.

2. Сделать endpoint, который будет принимать фамилию человека и выводить по нему всю сводную информацию.

3. Сделать endpoint, который принимает информацию о новом человеке и создает запись в БД.

4. Сделать endpoint, который выводит список всех пользователей.

5. Сделать endpoint, который изменяет информацию о пользователе.

6**. Создать возможность связывать людей в дружеские отношения. Дать возможность вывести всех друзей пользователя.

Необходимый инструментарий:
FastAPI, SQLite3*, Asyncpg (Без ORM)

Необходимо прислать исходный код и видео работы. Всю работу осуществлять через Swagger или аналоги.







________
* Можно использовать любую другую реляционную СУБД, но тогда необходимо предоставить docker-compose для поднятия всего проекта.

---



### Stack:

- [x] <a href="https://docs.sqlalchemy.org/en/20"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-plain.svg" alt="python" width="15" height="15"/>
  Python 3.10 <br/></a>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/fastapi/fastapi-plain.svg" alt="fastapi" width="15" height="15"/> Fastapi v.0.111.0 <br/>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-plain.svg" alt="docker" width="15" height="15"/> Docker Compose <br/>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-plain.svg" alt="postgresql" width="15" height="15"/> PostgreSQL 15.0 <br/>
- [x] <a href="https://docs.pydantic.dev/">🕳 Pydantic 2.7.4<br/></a>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/pytest/pytest-plain.svg" alt="redis" width="15" height="15"/> Pytest<br/>

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

### Run:

Copy `.env_example` as `.env`

### pre-commit hook

[Set up pre-commit](https://pre-commit.com)\
Each commit executes the hooks (automations) listed in
**. pre-commit-config.yaml**.
If it's not clear what error prevents you from committing, you can run the
hooks manually and see the errors:

* `pre-commit run --all-files`

### Commands to run the entire project in containers

* `$ docker compose up --build -d` - Create and run container
* `$ docker exec -it redos_backend /bin/sh -c "/app/init_db.sh"` - create start table
* `$ docker exec -it redos_backend /bin/sh -c "/app/migrate.sh"` - migrate
* `$ docker compose exec backend pytest` - Running tests
* `$ docker compose down --rmi all` - Stop and delete docker containers
* `$ docker compose exec backend bash` - Enter container backend
* `$ docker compose logs backend` - Show logs

##### Access

* http://127.0.0.1:8089
* http://127.0.0.1:8089/api/v1/docs/ - документация

local:

_Install requirements_

```
pip install fastapi[all]
pip install -r requirements.txt
```
