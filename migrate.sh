#!/bin/sh

clear

echo "------------------------------START MIGRATION------------------------------"

current_dir=$(pwd)
migration_directory="$current_dir/src/migrations/versions/"
temp_files_list=$(mktemp)

ls -tr $migration_directory > "$temp_files_list"

while IFS= read -r file; do
  file_path="$migration_directory$file"
  PGPASSWORD=$POSTGRES_PASSWORD psql -h "$POSTGRES_HOST" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f "$file_path"
done < "$temp_files_list"

rm "$temp_files_list"

echo "------------------------------MIGRATION COMPLETED------------------------------"
